var express = require('express')
var app = express();
const redis = require("redis");
const cache = redis.createClient();
var consolidate = require("consolidate");
require('dotenv').config();
const needle = require('needle');

const getUserTweets = async (userId, bearerToken) => {
    let userTweets = [];

    // we request the author_id expansion so that we can print out the user name later
    let params = {
        "max_results": 100,
        "tweet.fields": "created_at",
        "exclude": "retweets",
        "expansions": "author_id,attachments.media_keys"
    }

    const options = {
        headers: {
            "authorization": `Bearer ${bearerToken}`
        }
    }

    let hasNextPage = true;
    let nextToken = null;
    let userName;

    while (hasNextPage) {
        let resp = await getPage(userId, params, options, nextToken);
        if (resp && resp.meta && resp.meta.result_count && resp.meta.result_count > 0) {
            userName = resp.includes.users[0].username;
            if (resp.data) {
                userTweets.push.apply(userTweets, resp.data);
            }
            if(userTweets.length >= 200){
                hasNextPage = false;
            }
            if (resp.meta.next_token) {
                nextToken = resp.meta.next_token;
            } else {
                hasNextPage = false;
            }
        } else {
            hasNextPage = false;
        }
    }
    return userTweets;
}

const getPage = async (userId, params, options, nextToken) => {
    if (nextToken) {
        params.pagination_token = nextToken;
    }

    try {
        const resp = await needle('get', `https://api.twitter.com/2/users/${userId}/tweets`, params, options);

        if (resp.statusCode != 200) {
            return;
        }
        return resp.body;
    } catch (err) {
        throw new Error(`Request failed: ${err}`);
    }
}

app.use(express.static('static'))

var http = require('http').createServer(app);
http.listen(8881, () => {
    console.log('listening on *:8881');
});

app.set("views", __dirname + "/views");
app.set("view engine", "html");
app.engine("html", consolidate.nunjucks);

const twitterSignIn = require('twittersignin')({
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    accessToken: process.env.TWITTER_ACCESS_TOKEN,
    accessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

app.get("/", function (req, res) {
    res.render("index.html", {});
});


app.get("/signin", async (req, res) => {
    const response = await twitterSignIn.getRequestToken({
        oauth_callback: "https://twittera11yscore.dav.li/callback",
        //oauth_callback: "http://localhost:8881/callback",
        x_auth_access_type: "read",
    });
    const requestToken = response.oauth_token;
    const requestTokenSecret = response.oauth_token_secret;
    cache.set(`tokens-${requestToken}`, requestTokenSecret, 'EX', 5 * 60);
    const callbackConfirmed = response.oauth_callback_confirmed;
    res.redirect(302, `https://api.twitter.com/oauth/authorize?oauth_token=${requestToken}`);
});

app.get("/callback", async (req, res) => {
    const oauthVerifier = req.query.oauth_verifier;
    const requestToken = req.query.oauth_token;
    res.render("loading.html", {
        url: `/score?oauth_token=${requestToken}&oauth_verifier=${oauthVerifier}`
    });
});

app.get("/score", async (req, res) => {
    try {
        const oauthVerifier = req.query.oauth_verifier;
        const requestToken = req.query.oauth_token;
        const requestTokenSecret = await cache.get(`tokens-${requestToken}`);
        const response = await twitterSignIn.getAccessToken(requestToken, requestTokenSecret, oauthVerifier);
        const accessToken = response.oauth_token;
        const accessTokenSecret = response.oauth_token_secret;

        const Twit = require("twit");
        const t = new Twit({
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            access_token: accessToken,
            access_token_secret: accessTokenSecret,
        });

        t._getBearerToken(async function (error, bearerToken) {
            var account = await t.get('account/verify_credentials');
            var userTimeline = await getUserTweets(account.data.id_str, bearerToken);

            var tweetsWithAltText = [];
            var tweetsWithoutAltText = [];
            for (var tweet of userTimeline) {
                if (tweet.attachments && tweet.attachments.media_keys) {
                    var showResponse = await t.get('statuses/show/:id', {
                        id: tweet.id,
                        include_ext_alt_text: true,
                        tweet_mode: "extended"
                    });
                    var tweetWithMedia = showResponse.data;
                    if (tweetWithMedia.extended_entities && tweetWithMedia.extended_entities.media[0].ext_alt_text) {
                        tweetsWithAltText.push(tweetWithMedia);
                    } else {
                        tweetsWithoutAltText.push(tweetWithMedia);
                    }
                }
            }
            var score = 1;
            score = score - tweetsWithoutAltText.length * 0.05;
            if (score < 0) {
                score = 0;
            }
            var scoreLetter = 'A+';
            if (score < 0.1) {
                scoreLetter = 'F';
            } else if (score < 0.2) {
                scoreLetter = 'D-';
            } else if (score < 0.25) {
                scoreLetter = 'D';
            } else if (score < 0.3) {
                scoreLetter = 'D+';
            } else if (score < 0.35) {
                scoreLetter = 'C-';
            } else if (score < 0.4) {
                scoreLetter = 'C';
            } else if (score < 0.45) {
                scoreLetter = 'C+';
            } else if (score < 0.5) {
                scoreLetter = 'B-';
            } else if (score < 0.6) {
                scoreLetter = 'B';
            } else if (score < 0.7) {
                scoreLetter = 'B+';
            } else if (score < 0.8) {
                scoreLetter = 'A-';
            } else if (score < 0.9) {
                scoreLetter = 'A';
            }
            var result = {
                username: account.data.screen_name,
                stats: {
                    total: 200,
                    tweetsWithAltText: tweetsWithAltText.length,
                    tweetsWithoutAltText: tweetsWithoutAltText.length,
                    score: score,
                    scoreLetter: scoreLetter
                },
                data: {
                    tweetsWithAltText: tweetsWithAltText,
                    tweetsWithoutAltText: tweetsWithoutAltText,
                }
            };
            res.render("index.html", {
                result
            });
        });
    } catch (e) {
        console.log(e);
        res.render("error.html", {});
    }
});